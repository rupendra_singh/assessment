<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('employees', [App\Http\Controllers\EmployeeController::class, 'index']);

Route::get('employees/create', [App\Http\Controllers\EmployeeController::class, 'create']);
Route::post('employees/store', [App\Http\Controllers\EmployeeController::class, 'store']);
Route::get('employees/edit/{id}', [App\Http\Controllers\EmployeeController::class, 'show']);
Route::post('employees/update/{id}', [App\Http\Controllers\EmployeeController::class, 'update']);
Route::get('employees/delete/{id}', [App\Http\Controllers\EmployeeController::class, 'destroy']);


// 2nd Highest Salary
Route::get('employees/secondHighestSalary', [App\Http\Controllers\EmployeeController::class, 'secondHighestSalary']);

// 5th Highest Salary
Route::get('employees/fifthHighestSalary', [App\Http\Controllers\EmployeeController::class, 'fifthHighestSalary']);


// Average Salary by Department
Route::get('employees/averageSalaryByDepartment', [App\Http\Controllers\EmployeeController::class, 'averageSalaryByDepartment']);

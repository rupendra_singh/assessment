<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function department()
    {
        return $this->belongsTo(Department::class, 'dept_id', 'id');
    }

    public function setHobbiesAttribute($hobbies)
    {
        $this->attributes['hobbies'] = implode(',', (array)$hobbies);
    }

    public function getHobbiesAttribute($hobbies)
    {
        $array= preg_split('/[,]/', $hobbies);

        $hobs = [
            '1' => 'Reading',
            '2' => 'Cricket',
            '3' => 'Surfing',
            '4' => 'Swimming',
            '5' => 'Watching Movies',
        ];
        $hob2 = array();
        foreach ($array as  $value) {
            if (!empty($hobs[$value])) {
                $hob2[] = $hobs[$value];
            }
        }
        return implode(', ',$hob2);
        
    }
}

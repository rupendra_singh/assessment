<?php

namespace App\Repository;

use App\Models\Department;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeRepository
{
    public function getAll()
    {
        $employees = Employee::with('department')->get();
        return $employees;
    }

    public function create()
    {
        $departments = Department::all();
        return $departments;
    }
    
    public function store(array $employee)
    
    {
       
       $data = Employee::create($employee);
        return $data;
    }

    public function show($id)
    {
        $employee = Employee::with('department')->find($id);
        return $employee;
        
    }

    public function update($employee, array $data)
    {
        
        $employee = Employee::find($employee);
        $employee->update($data);
        return $employee;
    }

    public function destroy($employee)
    {
        $employee = Employee::find($employee);
        $employee->delete();
        return $employee;
    }

    public function secondHighestSalary()
    {
        $secondHighestSalary = Employee::where('salary', '>', '0')->orderBy('salary', 'desc')->offset(1)->take(1)->get();
        return $secondHighestSalary;
    }

    public function fifthHighestSalary()
    {
        $fifthHighestSalary = Employee::where('salary', '>', '0')->orderBy('salary', 'desc')->offset(4)->take
        (1)->get();
        
        return $fifthHighestSalary;
    }   
       

    public function avgSalary()
    {
        $avgSalary = Employee::where('salary', '>', '0')->avg('salary');
        return $avgSalary;
    }
}

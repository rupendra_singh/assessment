<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Employee;
use App\Repository\EmployeeRepository;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    private $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function index()
    {
        $employees=  $this->employeeRepository->getAll();
        return view('employees', compact('employees'));
    }
  

    public function create()
    {
        $departments= $this->employeeRepository->create();
        return view('createemployee', compact('departments'));
    }
    

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'dept_id' => 'required',
            'salary' => 'required|integer',
            'hobbies' => 'required|min:1|max:3',
        ],
        [
            'hobbies.required' => 'Hobbies is required',
            'hobbies.min' => 'Hobbies must be minimum 1',
            'hobbies.max' => 'Hobbies must be maximum 3',
        
        ]);
        
        $employee = $request->all();
        $employee['hobbies'] =$request->hobbies ? implode(',', (array)$request->hobbies) : '';
        $data = $this->employeeRepository->store($employee);
        return redirect('employees')->with('success', 'Employees are successfully added');
    }

    public function show($id)
    {
        $departments = Department::all();
        $data = $this->employeeRepository->show($id);
        return view('edit', compact('data','departments'));
    }

    public function update(Request $request)
    {   
        
        $employee = $request->route('id');
        $data = $request->all();
        $data['hobbies'] =$request->hobbies ? implode(',', (array)$request->hobbies) : '';
        $input =  $this->employeeRepository->update($employee,$data);
        return redirect('employees')->with('success', 'Employees are successfully updated');
    }

    public function destroy(Request $request)
    {
        $employee = $request->route('id');
        $input = $this->employeeRepository->destroy($employee);
        return redirect('employees')->with('success', 'Employees are successfully deleted');
    }
    
    public function secondHighestSalary()
    {
        $data = $this->employeeRepository->secondHighestSalary();
        return view('2highestsalary', compact('data'));
    }

    public function fifthHighestSalary()
    {
        $data = $this->employeeRepository->fifthHighestSalary();
        return view('5highestsalary', compact('data'));
    }

    public function averageSalaryByDepartment()
    {
        $data = $this->employeeRepository->avgSalary();
        return view('avgsalary', compact('data'));
    }


}

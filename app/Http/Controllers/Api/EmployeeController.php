<?php

namespace App\Http\Controllers\Api;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\EmployeeRepository;

class EmployeeController extends Controller
{
    private $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function index()
    {
        $employees=  $this->employeeRepository->getAll();
        return response()->json([
            'ResponseCode'=>200,
            'status'=>'success',
            'data'=>$employees
        ]);
    }

    public function create()
    {
        $departments= $this->employeeRepository->create();
        return response()->json([
            'ResponseCode'=>200,
            'status'=>'success',
            'data'=>$departments
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'dept_id' => 'required',
            'salary' => 'required|integer',
            'hobbies' => 'required|min:1|max:3',
        ],
        [
            'hobbies.required' => 'Hobbies is required',
            'hobbies.min' => 'Hobbies must be minimum 1',
            'hobbies.max' => 'Hobbies must be maximum 3',
        
        ]
    );

        $employee            = $request->all();
        $employee['hobbies'] =$request->hobbies ? implode(',', (array)$request->hobbies) : '';
        $data                = $this->employeeRepository->store($employee);
        return response()->json([
            'ResponseCode'=>200,
            'status'=>'success',
            'data'=>$data
        ]);
    }

    public function show($id)
    {
        $departments = Department::all();
        $data        = $this->employeeRepository->show($id);
        return response()->json([
            'ResponseCode'=>200,
            'status'=>'success',
            'data'=>$data
        ]);
    }

    public function update(Request $request)
    {
        $employee        = $request->route('id');
        $data            = $request->all();
        $data['hobbies'] =$request->hobbies ? implode(',', (array)$request->hobbies) : '';
        $input           =  $this->employeeRepository->update($employee, $data);
        return response()->json([
            'ResponseCode'=>200,
            'message'=>'Employee updated successfully',
            'data'=>$input
        ]);
    }

    public function destroy(Request $request)
    {
        $employee = $request->route('id');
        $input    = $this->employeeRepository->destroy($employee);
        return response()->json([
            'ResponseCode'=>200,
            'status'=>'success',
            'message'=>'Employee deleted successfully'
        ]);
    }
}

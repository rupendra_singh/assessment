@extends('layouts.app')

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Employees</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                  <a href="{{url('employees/create')}}" class="btn btn-primary mb-3" type="button">Add Employee</a>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Salary</th>
                    <th>Gender</th>
                    <th>Hobbies</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                      @foreach ($employees as $employee)
                  <tr>
                    <td>{{$employee->name}}</td>
                    <td>{{$employee->department->name}}</td>
                    <td>{{$employee->salary}}</td>
                    @if ($employee->gender == 'm')
                        <td>Male</td>    
                        @else
                        <td>Female</td>    
                    @endif
                    
                    <td>{{$employee->hobbies}}</td>
                    <td>
                        <a class="btn" href="{{url('employees/edit/'.$employee->id)}}"><i class="fa fa-edit"></i></a>
                        <a class="btn" href="{{url('employees/delete/'.$employee->id)}}"><i class="fa fa-trash"></i></a>
                </td>
                    
                  </tr>        
                      @endforeach
                  
                  
                  
                  
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Salary</th>
                    <th>Gender</th>
                    <th>Hobbies</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>

@endsection
@extends('layouts.app')

@section('content')

<section class="content">
    <div class="container-fluid">
        <form action="{{url('employees/store')}}" method="POST" id="quickForm">
        @csrf
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">

                    <div class="card card-primary">


                        <!-- form start -->

                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input  name="name" class="form-control" id="name"
                                    placeholder="Enter Emplyee Name">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="dept_id">Department</label>
                                <select name="dept_id" class="form-control">
                                    <option value="">Select Department</option>
                                    @foreach ($departments as $department)
                                    <option id="departments" value="{{$department->id}}">{{$department->name}}</option>
                                    @endforeach
                                </select>
                                @error('dept_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="salary">Salary</label>
                                <input type="text" name="salary" class="form-control" id="salary"
                                    placeholder="Enter Salary">
                                     @error('salary')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Gender </label>
                                <label class="radio-inline">
                                    <input class="m-2" type="radio" name="gender" value="m" >Male
                                </label>
                                <label class="radio-inline">
                                    <input class="m-2" type="radio" name="gender" value="f">Female
                                </label>

                            </div>

                            <div class="form-group">
                                <label for="hobbies">Hobbies</label>
                                <label class="checkbox-inline"><input id="hobbies" class="m-2" type="checkbox"
                                      name="hobbies[]"  value="1">Reading</label>
                                <label class="checkbox-inline"><input class="m-2" id="hobbies" type="checkbox"
                                     name="hobbies[]"   value="2">Cricket</label>
                                <label class="checkbox-inline"><input class="m-2" id="hobbies" type="checkbox"
                                       name="hobbies[]" value="3">Surfing</label>
                                <label class="checkbox-inline"><input class="m-2" id="hobbies" type="checkbox"
                                     name="hobbies[]"   value="4">Swimming</label>
                                <label class="checkbox-inline px-5"><input class="mr-2" id="hobbies"type="checkbox"
                                      name="hobbies[]"  value="5">Watching Movies</label>
                            </div>



                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
        </form>
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script>
$(function () {
  
  $('#quickForm').validate({
    rules: {
      name: {
        required: true,
        name: true,
      },
      dept_id: {
        required: true,
        
      },
        salary: {
            required: true,
            number: true,
        },
        hobbies: {
            required: true,
        },
    },
    messages: {
      name: {
        required: "Please enter a name",
        
      },
      dept_id: {
        required: "Department is required",
        
      },
        salary: {
            required: "Salary is required",
            number: "Salary must be a number",
        },
        hobbies: {
            required: "Hobbies min 1 or max 3 is required",
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection
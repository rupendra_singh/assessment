@extends('layouts.app')

@section('content')

<section class="content">
    <div class="container-fluid">
        <form action="{{url('employees/update/'. $data->id)}}" method="POST" id="quickForm">
            @csrf
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">

                    <div class="card card-primary">


                        <!-- form start -->

                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input name="name" class="form-control" id="exampleInputEmail1"
                                    placeholder="Enter Emplyee Name" value="{{$data->name}}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Department</label>
                                <select name="dept_id" class="form-control">
                                    <option value="">Select Department</option>


                                    @foreach ($departments as $key => $value)
                                    <option value="{{ $value->id }}" @if($value->id == $data->dept_id) selected @endif>{{
                                        $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Salary</label>
                                <input type="number" name="salary" class="form-control" id="exampleInputPassword1"
                                    placeholder="Enter Salary" value="{{$data->salary}}">
                            </div>
                            <div class="form-group">
                                <label>Gender </label>
                                <label class="radio-inline">
                                    <input class="m-2" type="radio" name="gender" value="m" {{ $data->gender == 'm'
                                    ? 'checked' : ''}} >Male
                                </label>
                                <label class="radio-inline">
                                    <input class="m-2" type="radio" name="gender" value="f" {{ $data->gender == 'f'
                                    ? 'checked' : ''}}>Female
                                </label>

                            </div>

                            
                            <div class="form-group">
                                <label>Hobbies</label>
                                <label class="checkbox-inline">
                                    <input class="m-2" type="checkbox" name="hobbies[]"
                                        value="1" {{strpos($data,'Reading')? 'checked':''}}>Reading
                                    </label>
                                <label class="checkbox-inline"><input class="m-2" type="checkbox" name="hobbies[]"
                                        value="2"{{strpos($data,'Cricket')? 'checked':''}}>Cricket</label>
                                <label class="checkbox-inline"><input class="m-2" type="checkbox" name="hobbies[]"
                                        value="3"{{strpos($data,'Surfing')? 'checked':''}}>Surfing</label>
                                <label class="checkbox-inline"><input class="m-2" type="checkbox" name="hobbies[]"
                                        value="4"{{strpos($data,'Swimming')? 'checked':''}}>Swimming</label>
                                <label class="checkbox-inline px-5"><input class="mr-2" type="checkbox" name="hobbies[]"
                                        value="5"{{strpos($data,'Watching Movies')? 'checked':''}}>Watching Movies</label>
                            </div>



                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
        </form>
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>

@endsection
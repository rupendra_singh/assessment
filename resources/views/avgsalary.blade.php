@extends('layouts.app')

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Employees</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                 
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    
                    <th>Average Salary by Department</th>
                    
                    <!-- <th>Action</th> -->
                  </tr>
                  </thead>
                  <tbody>
                    <td>{{$data}}</td>
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>

@endsection
@extends('layouts.app')

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Employees</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                 
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Salary</th>
                    <th>Gender</th>
                    <th>Hobbies</th>
                    <!-- <th>Action</th> -->
                  </tr>
                  </thead>
                  <tbody>
                     @foreach ($data as $tdata)
                         <td>{{$tdata->name}}</td>
                         <td>{{$tdata->department->name}}</td>
                            <td>{{$tdata->salary}}</td>
                            @if ($tdata->gender == 'm')
                        <td>Male</td>    
                        @else
                        <td>Female</td>    
                    @endif
                    
                    <td>{{$tdata->hobbies}}</td>

                     @endforeach
                  
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Salary</th>
                    <th>Gender</th>
                    <th>Hobbies</th>
                    <!-- <th>Action</th> -->
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>

@endsection